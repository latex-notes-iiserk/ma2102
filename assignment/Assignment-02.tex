\documentclass{scrartcl}
\usepackage{handout, xargs}

%%%% MACROS %%%%
\newcommandx{\M}[2][2 = {}]{\mathcal{M}_{#1 #2}}
\newcommand{\F}{\mathbb{F}}
\newcommand{\I}[1]{\mathbb{I}_{#1}}
\DeclareMathOperator{\col}{col}
\DeclareMathOperator{\row}{row}
\renewcommand*{\implies}{\Rightarrow}
\newcommand{\B}{\mathbb{B}}
\DeclareMathOperator{\Span}{span}
\newcommand{\GL}[1]{\mathrm{GL}_{#1}}

\title{Assignment 02: Solutions}
\subtitle{MA2102: Linear Algebra I}
\author{\textsc{Piyush Kumar Singh} \\ \texttt{22MS027} \\[-2pt] Group - C}
\date{\today}

\begin{document}
\maketitle

\begin{exercise}[10 points]
    Find the dimension of $\Span_\R S$, where
    \[
        S : = \qty{\mqty(1 \\ 1 \\ 0), \mqty(0 \\ 1 \\ 1), \mqty(1 \\ 4 \\ 3)}
    \]
\end{exercise}
\begin{solution}
    Observe that the set \(S\) is not linearly independent as
    \[
        \mqty(1 \\ 1 \\ 0) + 3 \mqty(0 \\ 1 \\ 1) = \mqty(1 \\ 4 \\ 3).
    \]
    So we can remove a vector from the set $S$, say $\mqty(1 & 4 & 3)$, to generate a linearly independent set $S'$.
    $$\implies \Span_\R S = \Span_\R S'.$$
    Since, span's are equal and set $S'$ is linearly independent then $\dim(\Span_\R S) = \abs{S'} = 2$.
\end{solution}

\begin{exercise}[5 + 5 points]
    Solve the following exercise:
    \begin{enumerate}[(i)]
        \item Let \(A = \mqty[1 & 1 & 1 \\ 2 & 2 & 3 \\ x & y & z]\) and let \(V = \qty{(x, y, z) \in \R^3 : \det(A) = 0}\). Show that \(V\) is a vector space and find the dimension of \(V\).

        \item Let $V = \qty{(x_1 - x_2 + x_3, x_1 + x_2 - x_3) : (x_1, x_2, x_3) \in \R^3}$. Show that $V$ is a vector space and find the dimension of $V$.
    \end{enumerate}
\end{exercise}
\begin{proof}
    For this problem, we require the definition of vector spaces.
    \begin{enumerate}[(i)]
        \item First, we have to construct the vector space $V$.
              \begin{align*}
                   & \det A = \mqty[1\,                                   & 1 & 1 \\ 2 & 2 & 3 \\ x & y & z] = 0 \\
                   & \implies \quad (2z - 3y) - (2z - 3x) + (2y - 2x) = 0         \\
                   & \implies \quad - y + x = 0                                   \\
                   & \implies \quad x = y.
              \end{align*}
              Now, we can write the vector space as
              $$
                  V = \qty{\mqty(x \\ x \\ z): x, z \in \R} = \qty{x\mqty(1 \\ 1 \\ 0) + z \mqty(0 \\ 0 \\1) : x, z \in \R}.
              $$
              And consider $S := \Span\qty{(1, 1, 0), (0, 0, 1)}$. By definition of $V$, it is clear that $V \subseteq S$.

              Let $\va{v} \in S$, then $\va{v} = \alpha (1, 1, 0) + \beta (0, 0, 1)$ for some $\alpha, \beta \in \R$. Furthermore, we can write $\va{v} = (\alpha, \alpha, \beta) \implies \va{v} \in V \implies S \subseteq V.$ And using the previous assertion, we can conclude that $V = S$. Since a set of vectors spans $V$, this directly proves that \underline{$V$ is a vector space.}

              Observe that the spanning set of $S$ is linearly independent
              \[
                  p(1, 1, 0) + q(0, 0, 1) = (0, 0, 0) \implies (p, p, q) = (0, 0, 0) \implies p = q = 0.
              \]
              $\implies$ spanning set is the basis for $V$,
              \[
                  \implies \quad \dim(V) = \abs{\qty{(1, 1, 0), (0, 0, 1)}} = 2.
              \]

        \item Observe that $V$ can be expressed as
              \[
                  V = \qty{x_1 \mqty(1 \\ 1) + x_2 \mqty(-1 \\ 1) + x_3 \mqty(1 \\ -1) : \mqty(x_1 \\ x_2 \\ x_3) \in \R^3}.
              \]
              Consider,
              $$
                  S := \Span\qty{\mqty(1 \\ 1), \mqty(-1 \\ 1), \mqty(1 \\ -1)}
              $$
              $V \subseteq S$ is evident from the definition of $V$. Let $\va{v} \in S$, then $\va{v} = \alpha (1, 1) + \beta (-1, 1) + \gamma (1, -1)$ for some $\alpha, \beta, \gamma \in \R$. Furthermore we can write $\va{v} = (\alpha - \beta + \gamma, \alpha + \beta - \gamma) \implies \va{v} \in V \implies S \subseteq V$. So, we can conclude that $V = S$. Since a set of vectors spans $V$, this directly proves that \underline{$V$ is a vector space.}

              Note that the spanning set of $V$ is linearly dependent
              \[
                  0\mqty(1 \\ 1) + \mqty(-1 \\ 1) + \mqty(1 \\ -1) = \mqty(0 \\ 0).
              \]
              So we can remove one vector from the spanning set to form a basis set, say $(1, -1)$,
              \[
                  \implies \quad \dim V = \abs{\qty{\mqty(1 \\ 1), \mqty(-1 \\ 1)}} = 2.
              \]
    \end{enumerate}
\end{proof}

\begin{exercise}[10 points]
    Let $A$ be an $n \times n$ real matrix such that the sum of the entries of each column of $A$ is $\lambda$. Show that \(\lambda\) is an eigenvalue of $A$.
\end{exercise}
\begin{proof}
    Let $A \in \M{n}(\R)$. And we can write
    \[
        A = \mqty[a_{ij}]_{1 \le i, j \le n}
    \]
    Since all columns sum to $\lambda$, we can write the following equation
    \[
        C_j = \sum_{i = 1}^{n} a_{ij} = \lambda \qc{\forall j \in \N_n}
    \]
    \begin{proposition}[$P$ and $P^T$ having same eigenvalue]\label{prop:A-AT}
        Let $P \in \M{n}(\R)$, then eigenvalues of $P$ and $P^T$ are same.
    \end{proposition}
    \noindent \textbf{Pf:} Discussed and proved in the last assignment. \hfill $\square$

    Consider $A^T$, and using the given hypothesis, we know that all the rows will sum to $\lambda$. Moreover, take vector $\va{x} = \mqty(1 & 1 & \cdots & 1) \in \R^n$.
    \begin{align*}
        \implies & \quad A^T \va{x} = \mqty[\sum_{i = 1}^{n} a_{i1} \\ \vdots \\ \sum_{i = 1}^{n} a_{in}] = \mqty[\lambda \\ \vdots \\ \lambda] = \lambda \mqty[1 \\ \vdots \\ 1]. \\
        \implies & \quad A^T \va{x} = \lambda \va{x}
    \end{align*}
    $\implies \lambda$ is one of the eigenvalues of $A^T$ and by \autoref{prop:A-AT} $\lambda$ is an eigenvalue of $A$.
\end{proof}

\begin{exercise}[12 points]
    For a prime $p$ and a natural number $n$, find the cardinality of $\GL{n}(\F_p)$.
\end{exercise}
\begin{solution}
    The $\GL{n}(\F_p)$ cardinality equals the number of basis sets of $\F^n_p$.

    Let $\B$ a basis set of $\F_p^n$ and given by
    \[
        \B := \qty{\va{b}_1, \va{b}_2, \cdots, \va{b}_n}.
    \]
    Since number of unique vectors in this space will be $p^n$, then for vector $\va{b}_1$, we have $p^n - 1$ choices as $\va{b}_1 \ne \va{0}$. Now consider $\va{b}_2$, we have $p^n - p$ choices as we have $p$ linear combinations of vector $\va{b}_1$.

    Now for vector $\va{b}_k$ we have $p^n - p^{k-1}$ choices, since we have ${k-1}$ vectors $\qty(\va{b}_1, \va{b}_2, \cdots, \va{b}_{k-1})$ so total linear combinations possible is $p^{k-1}$.

    So number of basis set of $\F^n_p$ and the cardinality of $\GL{n}(\F_p)$ is
    \[
        \fbox{$\prod_{i=0}^{n-1} (p^n - p^i)$}.
    \]
\end{solution}

\begin{exercise}[12 points]
    Let $V$ be a finite-dimensional real vector space and let $W_1, W_2, \cdots, W_n$ be proper subspaces of $V$. Show that
    \[
        V \ne \bigcup\limits_{i = 1}^{n} W_i.
    \]
\end{exercise}
\begin{proof}
    If $W_p \subseteq W_q$ for some $p, q \le n$, then $W_p \cup W_q = W_q$.
    \[
        \implies \quad \bigcup_{i = 1}^{n} W_i = \bigcup_{\substack{i = 1 \\ i \ne p}}^n W_i .
    \]
    $\implies$ We can remove all the subspaces $W_k$ such that
    \[
        W_k \subseteq \bigcup_{\substack{i = 1 \\ i \ne k}}^n W_i.
    \]
    Now re-index the remaining subspaces as $W_1', W_2', \cdots, W_m'$ here \underline{$m$ is minimal} such that
    \[
        W_k' \nsubseteq \bigcup_{\substack{i = 1 \\ i \ne k}}^m W_i'
    \]
    It is enough to show that
    \[
        V \ne \bigcup_{i = 1}^m W_i'.
    \]
    Suppose to contrary that the finite union of proper subspaces is equal to the total vector space.

    Since $W_m'$ is a proper subspace of $V$, then $\exists ~ \va{v} \notin W_m'$ and as $m$ is minimal then $\exists ~\va{w} \in W_m'$ such that $\va{w} \notin \bigcup_{i = 1}^{m - 1} W_i'$. Observe that $\va{v}, \va{w} \ne \va{0}$.

    Furthermore, define
    \[
        S := \qty{\va{v} + \lambda \va{w} : \lambda \in \R}.
    \]
    This definition of $S$ implies $S$ is infinite [$\because$ here field is infinite].
    \begin{claim}
        $S \cap W_m' = \emptyset$.
    \end{claim}
    \noindent \textbf{\sffamily Pf:} Suppose not, $S \cap W_m' \ne \emptyset \implies \exists ~\lambda \in \R$ such that $\va{v} + \lambda \va{w} \in W_m'$ and since $\va{w} \in W_m'$
    \begin{align*}
        \implies & \quad (\va{v} + \lambda \va{w}) - \lambda \va{w} \in W_m' \\
        \implies & \quad \va{v} \in W_m'.
    \end{align*}
    This contradicts the definition of $\va{v}$. \hfill $\square$

    \begin{claim}
        $\abs{S \cap W_i'} \le 1, ~\forall~ 1 \le i < m$.
    \end{claim}
    \noindent \textbf{\sffamily Pf:} Suppose not that for some $i$, the intersection contains more than one vector $\implies \exists \lambda_1, \lambda_2 \in \R$ such that $\lambda_1 \ne \lambda_2$ and $\qty(\va{v} + \lambda_1 \va{w}), \qty(\va{v} + \lambda_2 \va{w}) \in W_i'$.
    \begin{align*}
        \implies & \quad (\lambda_1 - \lambda_2)\va{w} \in W_i'    \\
        \implies & \quad \va{w} \in W_i' \qc{\text{for some} ~ i}.
    \end{align*}
    This contradicts the definition of $\va{w}$. \hfill $\square$

    By claims and given hypothesis, we can write
    \begin{align*}
        \implies & \quad \sum_{i = 1}^m \abs{S \cap W_i'} \le m - 1          \\
        \implies & \quad \abs{\bigcup_{i = 1}^m \qty(S \cap W_i')} \le m - 1 \\
        \implies & \quad \abs{S \cap \bigcup_{i = 1}^m W_i'} \le m - 1       \\
        % \intertext{}
        \implies & \quad \abs{S \cap V} \le m - 1                            \\
        \intertext{Since, $S$ is also a proper subspace of $V$,}
        \implies & \quad \abs{S} \le m - 1,
    \end{align*}
    but this gives us a contradiction as $S$ is an infinite set.
    \[
        \implies \quad \fbox{$V \ne \bigcup_{i = 1}^m W_i' = \bigcup_{j = 1}^n W_j$}.
    \]
\end{proof}
\end{document}