\documentclass[12pt]{scrartcl}
\usepackage{handout, xargs, cancel}
\usepackage{mlmodern}

%%%% MACROS %%%%
\newcommandx{\M}[2][2 = {}]{\mathcal{M}_{#1 #2}}
\newcommand{\F}{\mathbb{F}}
\newcommand{\I}[1]{\mathbb{I}_{#1}}
\DeclareMathOperator{\col}{col}
\DeclareMathOperator{\row}{row}
% \DeclareMathOperator{\ker}{ker}
\DeclareMathOperator{\im}{im}
\renewcommand*{\implies}{\Rightarrow}
\newcommand{\B}{\mathbb{B}}
\DeclareMathOperator{\Span}{span}
\newcommand{\GL}[1]{\mathrm{GL}_{#1}}
\DeclareMathOperator{\nullity}{nullity}
\newcommand{\bigzero}{\mbox{\normalfont\Large\bfseries 0}}

\title{Assignment 03: Solutions}
\subtitle{MA2102: Linear Algebra I}
\author{\textsc{Piyush Kumar Singh} \\ \texttt{22MS027} \\[-2pt] Group - C}
\date{\today}

\begin{document}
\maketitle

\begin{exercise}[10 points]
    Let $V$ be a finite-dimensional vector space and let $T: V \to V$ be a linear operator such that $\rank(T) = \rank(T^2)$. Show that $V = \ker(T) \oplus \im(T)$.
\end{exercise}
\begin{proof}
    Since $T: V \to V$ and $T^2(\va{v}) = T(T(\va{v}))$ then $T^2: V \to V$. So, by the rank nullity theorem, we have
    \begin{align*}
        \dim(V) & = \rank(T) + \nullity(T)     \\
        \dim(V) & = \rank(T^2) + \nullity(T^2)
    \end{align*}
    and with the given hypothesis, we can say that $\nullity(T) = \nullity(T^2)$.
    \begin{claim}\label{claim:kerT}
        $\ker(T) = \ker(T^2)$
    \end{claim}\noindent
    \textbf{\textsf{Pf}:} Let $\va{v} \in \ker(T)$ \ie $T(\va{v}) = \va{0}$. From here we can write
    \begin{align*}
        \implies \quad & T(T(\va{v})) = T(\va{0}) \\
        \implies \quad & T^2(\va{v}) = \va{0}     \\
        \implies \quad & \va{v} \in \ker(T^2)
    \end{align*}
    From the last statement we can say $\ker(T) \subseteq \ker(T^2) \implies \nullity(T) \le \nullity(T^2)$. But we have shown that they are equal which implies $\ker(T) = \ker(T^2)$.
    \hfill $\square$

    \begin{claim}
        $\ker(T)$ and $\im(T)$ are linearly independent.
    \end{claim}\noindent
    \textbf{\textsf{Pf}:} It is enough to show that their intersection has only one vector \ie $\va{0}$.

    Suppose not $\va{w} \ne \va{0} \in \ker(T) \cap \im(T)$. From its definition we have $\va{u} \in V$ such that $T(\va{u}) = \va{w}$. We can write
    \begin{align*}
        \implies \quad & T(T(\va{u})) = T(\va{w})
        \intertext{since, $\va{w} \in \ker(T)$ then,}
        \implies \quad & T^2(\va{u}) = \va{0}     \\
        \implies \quad & \va{u} \in \ker(T^2)
    \end{align*}
    From \autoref{claim:kerT}, $\va{u} \in \ker(T) \implies \va{w} = \va{0}$. And this contradicts the definition of $\va{w}$. \hfill $\square$

    Note, $\ker(T) \oplus \im(T) \subseteq V$, since both are subspaces of $V$, so we have $\dim(\ker(T) \oplus \im(T)) \le \dim(V)$. And $\dim(\ker(T) \oplus \im(T)) = \dim(\ker(T)) + \dim(\im(T))$, which is indeed equals to $\dim(V)$. \\
    $\implies V = \ker(T) \oplus \im(T)$.
\end{proof}

\begin{exercise}[10 points]
    Let $p$ be a prime, let $V$ be a finite-dimensional vector space over $\F_p$ and let $T: V \to V$ be a surjective linear operator. For $n \in N$, define $T^n : V \to V$ inductively by
    \[
        T^n(v) := T(T^{n-1}(v))
    \]
    for all $\va{v} \in V$, where $T^0$ denotes the identity map of $V$. Show that there exists an integer $m > 0$ such that $T^m = T^0$.
\end{exercise}
\begin{proof}
    Since our vector space is finite-dimensional and it is defined over a finite field. The total number of distinct vectors is also finite, $\abs{V} = p^n$, where $\dim V = n$.

    As we know, all the linear transformations have a matrix corresponding to it.\\
    Observe if one morphism is different from another then their matrix of linear transformation is different too. Using this we can find a total number of distinct linear maps possible from $V$ to $V$.

    Number of distinct linear maps = number of unique matrix $A \in \M{n}(\F_p)$ = $p^{n \times n} = p^{n^2}$.

    Extending this concept of distinct maps, we can find a total number of distinct isomorphisms from $V$ to $V$. We know that the matrix of a linear transformation of an isomorphism is invertible. So, the total number of distinct isomorphisms = cardinality of $\GL{n}(\F_p) = \prod_{i = 0}^{n-1} (p^n - p^i)$. Define,
    \[
        N := \prod_{i = 0}^{n-1} (p^n - p^i)
    \]
    \begin{claim}
        $T^m$ is an isomorphisms from $V$ to $V$ for all $m \in \N\naut = \qty{0, 1, 2, \dots}$
    \end{claim}
    \noindent \textbf{\textsf{Pf}:} We will prove this statement using induction on $m$.

    \noindent \textsl{Base Step:} \\
    For $m = 0$, it is given that $T^0$ is an identity map \ie $T^0$ is an isomorphism. \\
    For $m = 1$, we are given that $T$ is a surjective map. And since this is a mapping from $V$ to $V$, the dim of domain and co-domain are equal. We can conclude that $T$ is an isomorphism.

    \noindent \textsl{Induction Step:} \\
    Let's assume this statement holds good for $T^k$, so we have to show this is true for $T^{k+1}$. \\
    Let $\va{u}, \va{v} \in V$ such that $\va{u} \ne \va{v}$. Since we know $T$ and $T^{k-1}$ is an isomorphism so, $T^{k-1} (\va{u}) \ne T^{k-1} (\va{v})$. Now apply $T$ on these vectors
    \begin{align*}
        \implies \quad & T(T^{k-1} (\va{u}))  \ne T(T^{k-1} (\va{v})) \\
        \implies \quad & T^k (\va{u}) \ne T^k (\va{v})                \\
        \implies \quad & T^k ~\text{is injective.}
    \end{align*}
    Let $\va{w} \in V$, since $T$ is an isomorphism so $\exists~ \va*{\beta}$ such that $T(\va*{\beta}) = \va{w}$ and similarly $\exists ~ \va*{\alpha}$ such that $T^{k-1}(\va*{\alpha}) = \va*{\beta}$. So now we have,
    \begin{align*}
        T(\va*{\beta})            & = \va{w} \\
        T(T^{k-1} (\va*{\alpha})) & = \va{w} \\
        T^k (\va*{\alpha})        & = \va{w}
    \end{align*}
    Now we have $\va*{\alpha} \in V$ such that $T^k (\va*{\alpha}) = \va{w}$, now this shows that $T^k$ is a surjective linear map. This shows that $T^k$ is an isomorphism.

    Now this completes our proof that $T^m$ is an isomorphism from $V$ to $V$ for all $m \in \N\naut$. \hfill $\square$

    \noindent Now consider all the linear maps as $\qty{T^0, T, T^2, \dots, T^{N+1}}$, from Pigeon-Hole Principle we can say that two of the given linear isomorphisms will be the same. Say for $k_1, k_2 \in \N\naut$ with $k_1 \ne k_2$, we have $T^{k_1} = T^{k_2}$. WLOG $k_1 < k_2$,
    \begin{gather*}
        T^{k_1} (\va{x}) = T^{k_2} (\va{x}), \quad \forall ~ \va{x} \in V \\
        \implies \quad T^0 (\va{x}) = T^{k_2 - k_1} (\va{x}), \quad [\because ~\text{we can take inverse on both side}~ k_1 ~\text{times}]
    \end{gather*}
    Now, set $k_2 - k_1 =: m$ and we are done.
\end{proof}

\begin{exercise}[12 points]
    Let $n \in N$, let $V$ be a vector space of dimension $n$ and let $T: V \to V$ be a linear operator such that there exists a $\va{v} \in V$ with $T^n(v) = 0$ and $T^{n-1}(v) \ne 0$. Show that
    \[
        \B := (\va{v}, T(\va{v}), T^2(\va{v}), \dots, T^{n-1}(\va{v}))
    \]
    is a basis of $V$ and write down the matrix representation of $T$ w.r.t. the basis $\B$.
\end{exercise}
\begin{proof}
    Since $\dim V = n$ and we know $\abs{\B} = n$. So it is enough to show that $\B$ is linearly independent to claim that $\B$ is a basis of $V$.

    Suppose to contrary that $\B$ is linearly dependent, $\exists~ \qty{c_i}_{i = 1}^{n} \in \F$ with not all $c_i$'s zero, such that
    \begin{equation}
        c_1 \va{v} + c_2 T(\va{v}) + \cdots + c_n T^n(\va{v}) = \va{0} \label{eq:ind}
    \end{equation}
    \begin{claim}\label{claim:m>n}
        $T^m (\va{v}) = \va{0}, \quad \forall~ m \ge n$.
    \end{claim}
    \noindent \textbf{\textsf{Pf}:} From the definition of morphism we can write $T^m (\va{v}) = T^{m - n}(T^n(\va{v}))$. And we are given that $T^n(\va{v}) = \va{0} \implies T^m(\va{v}) = \va{0}, \quad ~ m \ge n$. \hfill $\square$

    \noindent Consider $T^{n-1}$ of whole equation \eqref{eq:ind},
    \begin{gather*}
        T^{n-1} \qty(c_1 \va{v} + c_2 T(\va{v}) + \cdots + c_n T^n(\va{v})) = T^{n-1}(\va{0}) \\
        c_1 T^{n-1} (\va{v}) + c_2 T^n (\va{v}) + \cdots + c_n T^{2 n - 2}(\va{v}) = \va{0}
        \intertext{from \autoref{claim:m>n},}
        c_1 T^{n-1} (\va{v}) = \va{0} \\
        \implies \quad ~ c_1 = 0, \qquad [\because~ T^{n-1}(\va{v}) \ne \va{0}]
    \end{gather*}
    With similar arguments, when we consider image of \eqref{eq:ind} under operator $T^{n-2}$, we get $c_2 = 0$.

    And repeating this argument, we can conclude that $c_i = 0, ~\forall ~ i \in \N_n = \qty{1, 2, \dots, n}$. And this gives us a contradiction since \eqref{eq:ind} was a linear relation. This proves that $\B$ is linearly independent and so $\B$ is the basis of $V$.

    For the matrix of linear transformation w.r.t. basis $\B$, say $A \in \M{n}(\F)$ is the matrix of linear transformation.
    \begin{align*}
        \qty[\B] A   & = \qty[T(\B)]                                                                                                       \\
        \mqty[\va{v} & T(\va{v})     & T^2(\va{v}) & \cdots & T^{n-1}(\va{v})] A & = \mqty[T(\va{v}) & T^2(\va{v}) & \cdots & T^n(\va{v})] \\
        \mqty[\va{v} & T(\va{v})     & T^2(\va{v}) & \cdots & T^{n-1}(\va{v})] A & = \mqty[T(\va{v}) & T^2(\va{v}) & \cdots & \va{0}]
    \end{align*}
    From the last equation, we can write the matrix of a linear transformation of $T$ w.r.t. basis $\B$ is
    \[
        A = \mqty(\begin{array}{@{}c|c@{}}
                \mqty{0        & \cdots  & 0} & 0 \\
                \hline
                \mqty{\I{n-1}} & \mqty{0          \\ \vdots \\ 0}
            \end{array})
    \]
\end{proof}

\begin{exercise}[10 points]
    Prove that every $m \times n$ matrix of rank 1 with entries from a field $\F$ has the form $A = X Y^T$, where $X \in \F^m$ and $Y \in \F^n$.
\end{exercise}
\begin{proof}
    We are given that $A \in \M{m}[\times n](\F)$ and we know there exists a homomorphism $T: \F^n \to \F^m$ defined as
    \[
        T(\va{u}) := A \va{u} % = \va{v}, \quad \forall ~ \va{u} \in \F^n ~ \text{for some} ~ \va{v} \in \F^n
    \]
    And we know $\rank(A) = 1 \implies \dim(\im(T)) = 1$. Let $\va{b} \in \F^m$ be the basis vector for $\im(T)$. Now we can write $\forall ~ \va{u} \in \F^n ~ \exists ~ k \in \F$ such that
    \begin{equation}
        T(\va{u}) = A \va{u} = k \va{b} \in \F^m \label{eq:q4-T} % , \quad \forall ~ \va{u} in \F^n 
    \end{equation}
    And we know this above equation means that a linear combination of columns of $A$ generates scalar multiples of $\va{b}$. Define,
    \[
        A = \mqty(A_1 & A_2 & \cdots & A_m), \quad \forall ~ A_i \in \F^n
    \]
    From \eqref{eq:q4-T}, we can say that $A_i = k_i \va{b}$ for some $k_i \in \F$. So,
    \begin{equation*}
        A = \mqty(k_1 \va{b} & k_2 \va{b} & \cdots & k_m \va{b}) = \va{b} \mqty(k_1 & k_2 & \cdots & k_m)
    \end{equation*}
    Define, $\va{k} = \mqty(k_1 \\ k_2 \\ \vdots \\ k_m) \in \F^m$. Then, this shows
    \[
        A = \va{b} \mqty(k_1 & k_2 & \cdots & k_m) = \va{b} \va{k}^T
    \]
    Define $X = \va{b}$ and $Y = \va{k}$ and this completes the proof that,
    \[
        A = X Y^T
    \]
\end{proof}

\begin{exercise}[12 points]
    Let $M$ be an $n \times n$ real matrix. Show that the $\rank(M) = \rank(M M^T)$.
\end{exercise}
\begin{proof}
    We are given that $M \in \M{n}(\R)$. Consider a homomorphism $T$ from $\R^n$ to $\R^n$ such that $M$ is the matrix of linear transformation. Then, $T: \R^n \xrightarrow{M} \R^n$.
    \begin{claim}\label{claim:ker=ker}
        $\ker(M) = \ker(M^T M)$
    \end{claim}
    \noindent \textbf{\textsf{Pf}:} Let $\va{x} \in \ker(M) \implies M \va{x} = \va{0}$. Using this fact we can write,
    \begin{align}
        \implies \quad & M^T M \va{x} = M^T \va{0} = \va{0}         \nonumber \\
        \implies \quad & \va{x} \in \ker(M^T M)        \nonumber              \\
        \implies \quad & \ker(M) \subseteq \ker(M^T M) \label{eq:M-MT}
    \end{align}
    Now, let $\va{y} \in \ker(M^T M) \implies M^T M \va{x} = \va{0}$. Using this we can write,
    \begin{align}
        \implies \quad & \va{y}^T M^T M \va{y} = \va{y}^T \va{0} = 0 \nonumber \\
        \implies \quad & (M \va{y})^T (M \va{y}) = 0                 \nonumber \\
        \implies \quad & \norm{M \va{y}}^2 = 0                                 \\
        \implies \quad & M \va{y} = \va{0}                           \nonumber \\
        \implies \quad & \va{y} \in \ker(M)                          \nonumber \\
        \implies \quad & \ker(M^T M) \subseteq \ker(M) \label{eq:MT-M}
    \end{align}
    From \eqref{eq:M-MT} and \eqref{eq:MT-M}, we can conclude that $\ker(M) = \ker(M^T M)$. \hfill $\square$

    \noindent From the \autoref{claim:ker=ker}, we can say $\nullity(M) = \nullity(M^T M)$. From, the rank nullity theorem we can conclude that
    $$
        \rank(M) = \rank(M^T M).
    $$
    \begin{claim}\label{claim:ker=ker-1}
        $\ker(M^T) = \ker(M M^T)$
    \end{claim}
    \noindent \textbf{\textsf{Pf}:} Let $\va{x} \in \ker(M^T) \implies M^T \va{x} = \va{0}$. Using this fact we can write,
    \begin{align}
        \implies \quad & M M^T \va{x} = M \va{0} = \va{0}         \nonumber \\
        \implies \quad & \va{x} \in \ker(M M^T)        \nonumber            \\
        \implies \quad & \ker(M^T) \subseteq \ker(M M^T) \label{eq:M-MT-1}
    \end{align}
    Now, let $\va{y} \in \ker(M M^T) \implies M M^T \va{x} = \va{0}$. Using this we can write,
    \begin{align}
        \implies \quad & \va{y}^T M M^T \va{y} = \va{y}^T \va{0} = 0   \nonumber \\
        \implies \quad & (M^T \va{y})^T (M^T \va{y}) = 0               \nonumber \\
        \implies \quad & \norm{M^T \va{y}}^2 = 0                                 \\
        \implies \quad & M^T \va{y} = \va{0}                           \nonumber \\
        \implies \quad & \va{y} \in \ker(M^T)                          \nonumber \\
        \implies \quad & \ker(M M^T) \subseteq \ker(M^T) \label{eq:MT-M-1}
    \end{align}
    From \eqref{eq:M-MT-1} and \eqref{eq:MT-M-1}, we can conclude that $\ker(M) = \ker(M^T M)$. \hfill $\square$

    \noindent From the \autoref{claim:ker=ker-1}, we can say $\nullity(M^T) = \nullity(M M^T)$. From, the rank nullity theorem we can conclude that
    $$
        \rank(M^T) = \rank(M M^T).
    $$

    \begin{claim}\label{claim:A=AT}
        Let a matrix $A \in \M{m}[\times n](\F)$. Then $\rank(A) = \rank(A^T)$.
    \end{claim}
    \noindent \textbf{\textsf{Pf}:} Proved and discussed in class. \hfill $\square$

    Using \autoref{claim:ker=ker}, \autoref{claim:ker=ker-1} \& \autoref{claim:A=AT}, we can conclude that
    \[
        \boxed{\rank(M) = \rank(M^T M) = \rank(M M^T) = \rank(M^T).}
    \]
\end{proof}
\end{document}