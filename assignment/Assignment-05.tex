\documentclass[12pt]{scrartcl}

\usepackage[linear, nofancy]{handout}

%% VECTORS %%
\renewcommand{\v}{\va{v}}
\renewcommand{\u}{\va{u}}
\newcommand{\w}{\va{w}}
\newcommand{\0}{\va{0}}

\title{Assignment 05: Solutions}
\subtitle{MA2102: Linear Algebra I}
\author{Piyush Kumar Singh \\ \texttt{22MS027} \\[-2pt] Group - C}
\date{November 20, 2023}

\begin{document}
\maketitle

\begin{exercisebox}
    \begin{exercise}[5 Points]
        Let $T$ be a linear operator on a finite-dimensional vector space $V$. Show that if every nonzero vector in $V$ is an eigenvector of $T$, then all the eigenvalues of $T$ are equal.
    \end{exercise}
\end{exercisebox}
\begin{proof}
    Suppose not we have two distinct eigenvalues $\lambda_1, \lambda_2$ with $\lambda_1 \ne \lambda_2$. And let $\v_1, \v_2 \in V$ are corresponding eigenvectors of $\lambda_1, \lambda_2$.
    \[
        T(\v_1) = \lambda_1 \v_1 \qquad ; \qquad T(\v_2) = \lambda_2 \v_2
    \]
    Since $\v_1, \v_2 \ne \0$, $\v_1 + \v_2 \in V$, so $\v_1 + \v_2 \ne \0$ [as these are eigenvectors of two distinct eigenvalues so they are linearly independent].

    \noindent By hypothesis, $T(\v_1 + \v_2) = \lambda(\v_1 + \v_2)$ for some $\lambda \in \F$. And from linearity of operator $T$, we have
    \begin{gather*}
        T(\v_1 + \v_2) = T(\v_1) + T(\v_2)               \\
        T(\v_1 + \v_2) = \lambda_1 \v_1 + \lambda_2 \v_2 \\
        \implies ~ \lambda(\v_1 + \v_2) = \lambda_1 \v_1 + \lambda_2 \v_2 \\
        \implies ~ (\lambda - \lambda_1) \v_1 + (\lambda - \lambda_2) \v_2 = \0
    \end{gather*}
    Since, $\v_1, \v_2$ are linearly independent so we have $\lambda = \lambda_1 = \lambda_2$, which contradicts our assumption.\\
    So, all eigenvalues are equal.
\end{proof}

\begin{exercisebox}
    \begin{exercise}[10 + 5 Points]
        Prove the following:\vspace{-1ex}
        \begin{enumerate}[(a)]
            \item Show that an $n \times n$ matrix is diagonalizable if and only if the sum of the geometric multiplicities of its eigenvalues is $n$.\vspace{-1ex}
            \item Show that an $n \times n$ complex matrix is diagonalizable if and only if, for each of its eigenvalue $\lambda$, the algebraic and the geometric multiplicities of $\lambda$ are equal.
        \end{enumerate}
    \end{exercise}
\end{exercisebox}
\begin{proof}
    For the following proofs,
    \begin{enumerate}[(a)]
        \item ($\implies$) Suppose the matrix is diagonalizable. Then we have `$n$' eigenvectors which forms a basis for $\F^n$ (\ie eigenbasis).\\
              Notice, $S:=$ the sum of the geometric multiplicities has an upper bound of `$n$' \ie $S \le n$.\\
              Suppose not $S \ne n$. Then, we can't get an eigenbasis; the matrix is not diagonalizable, which contradicts our hypothesis. \\
              So, the sum of the geometric multiplicities of its eigenvalues is $n$.\\

              \noindent ($\impliedby$) We have $S = n$, which means we have `$n$' linearly independent eigenvectors. And which forms eigenbasis of $\F^n$. So, the matrix is diagonalizable.

        \item ($\implies$) Since the matrix is diagonalizable, from the previous result, we know the sum of the geometric multiplicities of its eigenvalues is $n$. And we know,
              \[
                  n = \text{sum of geometric multiplicities} \le \text{sum of algebraic multiplicities}
              \]
              And note,
              \[
                  \text{sum of algebraic multiplicities} \le n \quad [\text{by characteristic polynomial}]
              \]
              So, the sum of the algebraic multiplicities of its eigenvalues is $n$.

              ($\impliedby$) Since the matrix is complex. So, the sum of the algebraic multiplicities will be $n$. And since algebraic and geometric multiplicities are equal.\\
              $\implies ~$ the sum of geometric multiplicities $= n$.\\
              $\implies ~$ From the above result, the matrix is diagonalizable.
    \end{enumerate}
\end{proof}

\begin{exercisebox}
    \begin{exercise}[5 + 10 Points]
        Let $T$ be the linear operator on the vector space of $n \times n$ real matrices such that
        $$T(A) = A\trans$$\vspace{-6ex}
        \begin{enumerate}[(a)]
            \item Find the eigenvalues of the linear operator $T$.\vspace{-1ex}
            \item Is $T$ diagonalizable? Justify your answer.
        \end{enumerate}
    \end{exercise}
\end{exercisebox}
\begin{proof}
    \begin{enumerate}[(a)]
        \item Let $\lambda$ be the eigenvalue of $T$ with eigenvector $A \in \M{n}(\R)$, then
              \[
                  T(A) = A\trans = \lambda A
              \]
              Note, $A\trans \in \M{n}(\R) ~ \implies ~ T(A\trans) = (A\trans)\trans = \lambda(\lambda A)$.\\
              So we have $A = \lambda^2 A ~ \implies ~ \lambda^2 = 1$.\\
              $1, -1$ are eigenvalues of $T$.

        \item It is enough to show that every matrix in $\M{n}(\R)$ can be represented as a linear combination of eigenvectors.\\
              Let $A \ne 0 \in \M{n}(\R)$.
              $$
                  A = \qty(\frac{A + A\trans}{2}) + \qty(\frac{A - A\trans}{2})
              $$
              Observe,
              \[
                  \frac{A + A\trans}{2} \in E(1) \qquad ; \qquad \frac{A - A\trans}{2} \in E(-1)
              \]
              $\implies$ T is diagonalizable.
    \end{enumerate}
\end{proof}

\begin{exercisebox}
    \begin{exercise}[10 + 8 Points]
        Let $\P_n(x)$ be a real vector space of polynomials in $x$ of degree less than or equal to $n$. Let $r_0, r_1, \dots, r_n$ be distinct real numbers and for $i \in \qty{0, 1, \dots, n}$, let $L_{r_i} : \P_n(x) \to \R$ denote the evaluation map at $r_i$.
        \begin{enumerate}[(a)]
            \item Show that $\B^* := \qty{L_{r_0} , L_{r_1} , \dots, L_{r_n} }$ is a basis of the dual of $\P_n(x)$.
            \item Write down a basis $\B$ of $\P_n(x)$ such that $\B^*$ is its dual basis. Justify your answer.
        \end{enumerate}
    \end{exercise}
\end{exercisebox}
\begin{proof}
    Let a polynomial $p(x) \in \P_n(x)$ such that
    \[
        p(x) = a_0 + a_1 x + a_2 x^2 + \cdots + a_n x^n
    \]
    So, $L_{r_i} : \P_n(x) \to \R$ as
    \[
        L_{r_i}(p(x)) := p(r_i) \quad \forall ~ i \in \qty{0, 1, 2, \dots, n}
    \]
    \begin{enumerate}[(a)]
        \item Let $L = 0$ be a linear functional from $\P_n(x) \to \R$. And let
              \[
                  L = c_0 L_{r_0} + c_1 L_{r_1} + \cdots + c_n L_{r_n}
              \]
              for some $c_0, c_1, \dots, c_n \in \R$. For linear independence of $l_{r_i}$'s, it is enough to show that $c_i$'s are zero.\\
              We have a basis $\EE$ of $\P_n(x)$ defined as,
              \[
                  \EE := \qty(1, x, x^2, \dots, x^n) = \qty(e_0, e_1, e_2, \dots, e_n)
              \]
              \begin{itemize}[--]
                  \item $L(e_0) = c_0 L_{r_0}(e_0) + c_1 L_{r_1}(e_0) + \cdots + c_n L_{r_n}(e_0) = 0$
                        \[
                            \implies ~ c_0 + c_1 + \cdots + c_n = 0
                        \]
                  \item $L(e_1) = c_0 L_{r_0}(e_1) + c_1 L_{r_1}(e_1) + \cdots + c_n L_{r_n}(e_1) = 0$
                        \[
                            \implies ~ c_0 r_0 + c_1 r_1 + \cdots + c_n r_n = 0
                        \]
                  \item $L(e_2) = c_0 L_{r_0}(e_2) + c_1 L_{r_1}(e_2) + \cdots + c_n L_{r_n}(e_2) = 0$
                        \[
                            \implies ~ c_0 r_0^2 + c_1 r_1^2 + \cdots + c_n r_n^2 = 0
                        \]
                        And continuing like this for all $e_i$'s.
                  \item $L(e_n) = c_0 L_{r_0}(e_n) + c_1 L_{r_1}(e_n) + \cdots + c_n L_{r_n}(e_n) = 0$
                        \[
                            \implies ~ c_0 r_0^n + c_1 r_1^n + \cdots + c_n r_n^n = 0
                        \]
              \end{itemize}
              Combining these $n+1$ linear equations, we get
              \[
                  \underbrace{\mqty(
                      1 & 1 & \cdots & 1 \\
                      r_0 & r_1 & \cdots & r_n \\
                      r_0^2 & r_1^2 & \cdots & r_n^2 \\
                      \vdots & \vdots & {} & \vdots \\
                      r_0^n & r_1^n & \cdots & r_n^n
                      )}_{V_{n+1}} \mqty(c_0 \\ c_1 \\ c_2 \\ \vdots \\ c_n) = \mqty(0 \\ 0 \\ 0 \\ \vdots \\ 0)
              \]
              \begin{claim}
                  $\det(V_{n+1}) = \prod_{1 \le i \le n} (r_i - r_0) \det(V_n)$
              \end{claim}
              %   We can row reduce this matrix for computing the determinant,
              \begin{Proof}
                  \begin{align*}
                             & \mqty(
                      1      & 1                  & \cdots & 1                                                                       \\
                      r_0    & r_1                & \cdots & r_n                                                                     \\
                      r_0^2  & r_1^2              & \cdots & r_n^2                                                                   \\
                      \vdots & \vdots             & {}     & \vdots                                                                  \\
                      r_0^n  & r_1^n              & \cdots & r_n^n
                      ) \xrightarrow{R_2 \mapsto R_2 - r_0 R_1}
                      \mqty(
                      1      & 1                  & \cdots & 1                                                                       \\
                      0      & r_1-r_0            & \cdots & r_n-r_0                                                                 \\
                      r_0^2  & r_1^2              & \cdots & r_n^2                                                                   \\
                      \vdots & \vdots             & {}     & \vdots                                                                  \\
                      r_0^n  & r_1^n              & \cdots & r_n^n
                      ) \xrightarrow{R_3 \mapsto R_3 - r_0 R_2 - r_0^2 R_1}                                                          \\[10pt]
                             & \mqty(
                      1      & 1                  & \cdots & 1                                                                       \\
                      0      & r_1-r_0            & \cdots & r_n-r_0                                                                 \\
                      0      & r_1(r_1-r_0)       & \cdots & r_n(r_n-r_0)                                                            \\
                      \vdots & \vdots             & {}     & \vdots                                                                  \\
                      r_0^n  & r_1^n              & \cdots & r_n^n
                      ) \cdots \xrightarrow[\text{Do it $n-2$ times}]{R_i \mapsto R_i - r_0 R_{i-1} - \cdots - r_0^{i-1} R_1} \cdots \\[10pt]
                             & \mqty(
                      1      & 1                  & \cdots & 1                                                                       \\
                      0      & r_1-r_0            & \cdots & r_n-r_0                                                                 \\
                      0      & r_1(r_1-r_0)       & \cdots & r_n(r_n-r_0)                                                            \\
                      \vdots & \vdots             & {}     & \vdots                                                                  \\
                      0      & r_1^{n-1}(r_1-r_0) & \cdots & r_n^{n-1}(r_n-r_0)
                      )
                  \end{align*}
                  Now, we have
                  \[
                      \det(V_{n+1}) = \mdet{
                      1 & 1 & \cdots & 1 \\
                      0 & r_1-r_0 & \cdots &r_n-r_0 \\
                      0 & r_1(r_1-r_0) & \cdots & r_n(r_n-r_0) \\
                      \vdots & \vdots & {} & \vdots \\
                      0 & r_1^{n-1}(r_1-r_0) & \cdots & r_n^{n-1}(r_n-r_0)
                      }%_{(n+1) \times (n+1)} =
                      = \mdet{
                      r_1-r_0 & \cdots & r_n-r_0 \\
                      r_1(r_1-r_0) & \cdots & r_n(r_n-r_0) \\
                      \vdots & {} & \vdots \\
                      r_1^{n-1}(r_1-r_0) & \cdots & r_n^{n-1}(r_n-r_0)
                      }
                  \]
                  From the properties of determinants,
                  \[
                      \det(V_{n+1}) = (r_1 - r_0)(r_2 - r_0) \cdots (r_n - r_0) \underbrace{\mdet{
                      1          & 1                  & \cdots & 1 \\
                      r_1        & r_2                & \cdots & r_n \\
                      r_1^2      & r_2^2              & \cdots & r_n^2 \\
                      \vdots     & \vdots             & {}     & \vdots \\
                      r_1^{n-1}  & r_2^{n-1}          & \cdots & r_n^{n-1}
                      }}_{\det(V_n)}
                  \]
              \end{Proof}
              \begin{claim}
                  $\det(V_{n+1}) = \prod_{0 \le i < j \le n} (r_j - r_i)$
              \end{claim}
              \begin{Proof}
                  We can prove this by induction.\\
                  \underline{\textsf{Base Step}:} $n = 1$
                  \begin{gather*}
                      \det(V_2) = \prod_{0 \le i < j \le 1} (r_j - r_i) = (r_1 - r_0) \\
                      V_2 = \mqty(1 & 1 \\ r_0 & r_1) ~ \implies ~ \det(V_2) = (r_1 - r_0)
                  \end{gather*}
                  \underline{\textsf{Induction Step}:} Let this holds true for $k \ge 1 \in \N$, we have to show it holds for $k+1$. \\
                  And we have already shown that,
                  \[
                      \det(V_{k+1}) = \prod_{1 \le i \le k} (r_i - r_0) \cdot \det(V_k)
                  \]
                  By induction hypothesis,
                  \begin{gather*}
                      \det(V_{k+1}) = (r_1 - r_0)(r_2 - r_0) \cdots (r_k - r_0) \cdot \prod_{1 \le i < j \le k} (r_j - r_i) \\
                      \det(V_{k+1}) = \prod_{0 \le i < j \le k} (r_j - r_i)
                  \end{gather*}
              \end{Proof}
              Since $r_i$'s are distinct real numbers,
              \[
                  \det(V_{n+1}) \ne 0
              \]
              $\implies ~$ Matrix is invertible.\\
              $\implies ~$ This linear equation system only has a trivial solution.
              \[
                  \implies ~ c_0 = c_1 = \cdots = c_n = 0
              \]
              $\implies ~ \B^*$ is a linearly independent set of linear functionals. \\
              We know $\dim(V^*) = \dim(V)$ and $\dim(\P_n(x)) = n+1$.
              \[
                  \implies ~ \dim(\P_n^*(x)) = n+1 \qquad ; \qquad \abs{\B^*} = n+1
              \]
              $\implies ~ \B^*$ is a basis of $\P_n^*(x)$.

        \item Let $\B$ be an ordered set of $n+1$ polynomials from $\P_n(x)$ defined by
              \[
                  \B := \qty(p_0, p_1, \dots, p_n)
              \]
              and
              \[
                  p_i := \prod_{\substack{0 \le j \le n \\ i \ne j}} \frac{x - r_j}{r_i - r_j} =
                  \frac{(x - r_0)}{(r_i - r_0)} \cdots \frac{(x - r_{i-1})}{(r_i - r_{i-1})} \frac{(x - r_{i+1})}{(r_i - r_{i+1})} \cdots \frac{(x - r_n)}{(r_i - r_n)}
              \]
              For $\B$ to be the basis of $\P_n(x)$, it is enough to show that $p_i$'s are linearly independent.\\
              Let $p \in \P_n(x)$ such that
              \[
                  p = b_0 p_0 + b_1 p_1 + \cdots + b_n p_n = 0
              \]
              Apply $L_{r_0}$,
              \[
                  L_{r_0} (p)  = b_0 p_0(r_0) + b_1 p_1(r_0) + \cdots + b_n p_n(r_0) = 0
              \]
              Note,
              \[
                  p_i(r_j) = \delta_{ij} = \begin{cases}
                      1, \quad i = j \\
                      0, \quad \text{otherwise}
                  \end{cases}
              \]
              So we get,
              \[
                  \implies ~ L_{r_0} (p) = p(r_0) = b_0 = 0
              \]
              Similarly, we will get,
              \[
                  b_0 = b_1 = \cdots = b_n = 0
              \]
              by applying $L_{r_i}$'s one-by-one.\\
              $\implies ~ p_i$'s are linearly independent.
              And,
              \[
                  L_{r_i} (p_j) = \delta_{ij}
              \]
              $\implies ~ \B^*$ is a dual basis of $\P_n(x)$ with respect to basis $\B$.
    \end{enumerate}
    \underline{\textsf{Another Method for Exercise 4 (a)}}\\[1.2ex]
    Since $L_{r_i} : \P_n(x) \to \R$ and this works for all polynomials in $\P_n$. Let $L \in \P^*_n(x)$ and $L = 0$, and with the previous assumption we can write,
    \[
        L = c_0 L_{r_0} + c_1 L_{r_1} + \cdots + c_n L_{r_n} = 0
    \]
    Apply this zero linear functional on elements of $\B$.\\
    -- $L(p_0) = c_0 L_{r_0}(p_0) + c_1 L_{r_1}(p_0) + \cdots + c_n L_{r_n}(p_0) = 0$
    \begin{gather*}
        L(p_0) = c_0 p_0(r_0) + c_1 p_0(r_1) + \cdots + c_n p_0(r_n) \\
        L(p_0) = c_0 \\
        \implies ~ c_0 = 0
    \end{gather*}
    With a similar procedure, we have\\
    -- $L(p_i) = 0 ~ \implies ~ \boxed{c_i = 0}$.\\
    $\implies ~ L_{r_i}$'s are linearly independent.\\
    \note{we don't require linear independence of $\B$ for this method to work.}
\end{proof}

\end{document}