\documentclass[12pt]{scrartcl}

\usepackage[linear, nofancy]{handout}

%% VECTORS %%
\renewcommand{\v}{\vb{v}}
\renewcommand{\u}{\vb{u}}
\newcommand{\w}{\vb{w}}
\newcommand{\e}[1]{\hat{\vb{e}}_{#1}}
\newcommand{\0}{\vb{0}}

\DeclarePairedDelimiter\Bilinear{s(}{)}

\title{Assignment 06: Solutions}
\subtitle{MA2102: Linear Algebra I}
\author{Piyush Kumar Singh \\ \texttt{22MS027} \\[-2pt] Group - C}
\date{November 22, 2023}

\begin{document}
\maketitle

\begin{exercisebox}
    \begin{exercise}[10 points]
        Which of the following functions $s$, defined on vectors $\v = (v_1, v_2)$ and $\w = (w_1, w_2)$ in $\R^2$, are bilinear forms? justify your answers.
        \begin{enumerate}[(a)]
            \item $s(\v, \w) = 1$ \vspace{-1.25ex}
            \item $s(\v, \w) = (v_1 - w_1)^2 + v_2 w_2$ \vspace{-1.25ex}
            \item $s(\v, \w) = (v_1 + w_1)^2 - (v_1 - w_1)^2$ \vspace{-1.25ex}
            \item $s(\v, \w) = v_1 w_2 - v_2 w_1$
        \end{enumerate}
    \end{exercise}
\end{exercisebox}

\begin{proof}
    This exercise requires the definition of ``bilinear form''.
    \begin{definition*}[Bilinear Form]
        Let $V$ be a vector space over the field $\F$. A \textbf{bilinear form} on $V$ is a function $\bilinear{,} : V \times V \to \F$, % defined by
        % \[
        %     \bilinear{\v, \w} := \qty(\overline{\vb{x}})\trans A \vb{y} = \vb{x}\herm A \vb{y}
        % \]
        and which satisfies following axioms\vspace{-1.25ex}
        \begin{itemize}[$\cdot$]
            \item $\bilinear{c \v, \w} = \bar{c} \bilinear{\v, \w}, \quad \bar{c} ~\text{is the complex conjugate of}~ c$ \vspace{-1.25ex}
            \item $\bilinear{\v + \v', \w} = \bilinear{\v, \w} + \bilinear{\v', \w}$ \vspace{-1.25ex}
            \item $\bilinear{\v, c \w} = c \bilinear{\v, \w} $\vspace{-1.25ex}
            \item $\bilinear{\v, \w + \w'} = \bilinear{\v, \w} + \bilinear{\v, \w'}$ \vspace{-1.25ex}
        \end{itemize}
        for all $\v, \v', \w, \w' \in V$ and $c \in \F$. In other words, $\bilinear{,}$ is linear in each argument.
    \end{definition*}
    \begin{enumerate}[(a)]
        \item This function is \textsc{not} a bilinear form on $\R^2$. Let $\v, \w (\ne \0) \in \R^2$ and $\alpha (> 1) \in \R$. \\
              Define $\u := \alpha \v$,
              \begin{align*}
                  \Bilinear{\u, \w} & = 1                        \\
                                    & = \Bilinear{\alpha \v, \w} \\
                                    & = \alpha \Bilinear{\v, \w} \\
                                    & = \alpha                   \\
                  \implies ~ \alpha & = 1
              \end{align*}
              This is a contradiction as $\alpha \ne 1$.

        \item This function is \textsc{not} a bilinear form on $\R^2$. Let $\v, \w (\ne \0) \in \R^2$.
              \begin{align*}
                  \Bilinear{2 \v, \w} & = (2v_1 - w_1)^2 + 2 v_2 w_2                \\
                                      & = 4v_1^2 + w_1^2 - 4 v_1 w_1 + 2 v_2 w_2
                  \intertext{and if we calculate $2 \Bilinear{\v, \w}$}
                  2 \Bilinear{\v, \w} & = 2(v_1^2 + w_1^2 - 2 v_1 w_1 + v_2 w_2)    \\
                                      & = 2 v_1^2 + 2 w_1^2 - 4 v_1 w_1 + 2 v_2 w_2
              \end{align*}
              Since $\Bilinear{2 \v, \w} \ne 2 \Bilinear{\v, \w}$.
        \item This function is a bilinear form on $\R^2$. Observe $\Bilinear{\v, \w} = 4 v_1 w_1$. \\
              Let $\v, \v', \w, \w' (\ne \0) \in \R^2$ and $a, b, c, d \in \R$.
              \begin{align*}
                  \Bilinear{a \v + b \v', c \w + d \w'} & = 4 (a v_1 + b v_1') (c w_1 + d w_1')                                                                \\
                                                        & = 4 a c v_1 w_1 + 4 a d v_1 w_1' + 4 b c v_1' w_1 + 4 b d v_1' w_1'                                  \\
                                                        & = a c~\Bilinear{\v, \w} + a d~\Bilinear{\v, \w'} + b c~\Bilinear{\v', \w} + b d ~\Bilinear{\v', \w'} \\
                                                        & = 4 (a v_1)(c w_1) + 4 (a v_1)(d w_1') + 4 (b v_1')(c w_1) + 4 (b v_1')(d w_1')                      \\
                                                        & = \Bilinear{a \v, c \w} + \Bilinear{a \v, d \w'} + \Bilinear{b \v', c \w} + \Bilinear{b \v', d \w'}
              \end{align*}
              This shows that $\Bilinear{,}$ is linear in each argument.
        \item This function is a bilinear form on $\R^2$. \\
              Let $\v, \v', \w, \w' (\ne \0) \in \R^2$ and $a \in \R$.
              \begin{itemize}[$\odot$]
                  \item $\Bilinear{a \v, \w} = a v_1 w_2 - a v_2 w_1 = a(v_1 w_2 - v_2 w_1) = a ~\Bilinear{\v, \w}$.
                  \item
                        \begin{align*}
                            \Bilinear{\v + \v', \w} & = (v_1 + v_1')w_2 - (v_2 + v_2')w_1           \\
                                                    & = (v_1 w_2 - v_2 w_1) + (v_1' w_2 - v_2' w_1) \\
                                                    & = \Bilinear{\v, \w} + \Bilinear{\v', \w}
                        \end{align*}
                  \item $\Bilinear{\v, a \w} = a v_1 w_2 - a v_2 w_1 = a(v_1 w_2 - v_2 w_1) = a ~\Bilinear{\v, \w}$.
                  \item \begin{align*}
                            \Bilinear{\v, \w + \w'} & = v_1 (w_2 + w_2') - v_2 (w_1 + w_1')         \\
                                                    & = (v_1 w_2 - v_2 w_1) + (v_1 w_2' - v_2 w_1') \\
                                                    & = \Bilinear{\v, \w} + \Bilinear{\v, \w'}
                        \end{align*}
              \end{itemize}
              This proves that $\Bilinear{,}$ is linear in both arguments.
    \end{enumerate}
\end{proof}

\begin{exercisebox}
    \begin{exercise}[10 points]
        Let $V$ be a finite-dimensional real vector space and let $\bilinear{,}: V \times V \to \R$ be a positive definite symmetric bilinear form. Show that for every linear functional $f: V \to \R$, there exists a unique $\v_f \in V$ such that for all $\v \in V$, we have
        $$
            f(\v) = \bilinear*{\v_f , \v}.
        $$
    \end{exercise}
\end{exercisebox}

\begin{proof}
    Consider $\hat{\B} := \qty(\e{1}, \e{2}, \cdots, \e{n})$ be an orthonormal basis of $V$ with respect to the form $\bilinear{,}$. Since $\hat{\B}$ is a basis, so for all $\v \in V$, we have
    \begin{gather*}
        \v = c_1 \e{1} + c_2 \e{2} + \cdots + c_n \e{n}
        \intertext{for some $c_1, c_2, \dots, c_n \in \R$. Using the orthonormal basis and given bilinear form}
        \v = \bilinear{\v, \e{1}} \e{1} + \bilinear{\v, \e{2}} \e{2} + \cdots + \bilinear{\v, \e{n}} \e{n}
    \end{gather*}
    Observe
    \begin{align*}
        f(\v) & = f\qty(c_1 \e{1} + c_2 \e{2} + \cdots + c_n \e{n})                                                         \\
              & = c_1 f(\e{1}) + c_2 f(\e{2}) + \cdots + f(\e{n})                                                           \\
              & = \bilinear{\v, \e{1}} f(\e{1}) + \bilinear{\v, \e{2}} f(\e{2}) + \cdots + \bilinear{\v, \e{n}} f(\e{n})    \\
              & = \bilinear*{\v, f(\e{1}) \e{1}} + \bilinear*{\v, f(\e{2}) \e{2}} + \cdots + \bilinear*{\v, f(\e{n}) \e{n}} \\
              & = \bilinear*{\v, f(\e{1}) \e{1} + f(\e{2}) \e{2}+ \cdots + f(\e{n}) \e{n}}
    \end{align*}
    Define \fbox{$\v_f := f(\e{1}) \e{1} + f(\e{2}) \e{2}+ \cdots + f(\e{n}) \e{n}$}. Now we have
    \[
        f(\v) = \bilinear{\v, \v_f} = \bilinear{\v_f, \v}
    \]
    Uniqueness:\\
    Let $\v_1, \v_2 \in V$ be two vectors such that
    \[
        f(\u) = \bilinear{\u, \v_1} = \bilinear{\u, \v_2} \quad \forall ~ \u \in V
    \]
    Since this holds for every vector, let $\u (\ne \0) \in V$
    \begin{align*}
        0 & = f(\u) - f(\u)                             \\
          & = \bilinear{\u, \v_1} - \bilinear{\u, \v_2} \\
        0 & = \bilinear{\u, \v_1 - \v_2}
    \end{align*}
    We have only one option \ie $\v_1 - \v_2 = \0 \implies \v_1 = \v_2$. This completes the proof of the uniqueness of this representation.
\end{proof}

\begin{exercisebox}
    \begin{exercise}[10 points]
        Let $V$ be a finite dimensional complex vector space, let $\bilinear{,}: V \times V \to \C$ be a positive definite Hermitian form and for all $\v \in V$, let $\norm{\v}:= \sqrt{\bilinear*{\v, \v}}$. Show that for all $\v, \w \in V$, we have
        \[
            \abs{\bilinear*{\v, \w}} \le \norm{\v} ~ \norm{\w}.
        \]
    \end{exercise}
\end{exercisebox}

\begin{proof}
    If either $\v$ or $\w$ is a zero-vector, there is nothing to prove.\\ % We can prove this inequality in two different cases.\\
    % Case 1 ($\w = a \v$ for some $a (\ne 0) \in \C$):
    % \begin{align*}
    %     \abs{\bilinear{\v, \w}} & = \abs{\bilinear{\v, a \v}}                                    \\
    %                             & = a \bilinear{\v, \v}                                          \\
    %                             & = \sqrt{\bilinear{\v, \v}} ~ a \sqrt{\bilinear{\v, \v}}        \\
    %     % & = \sqrt{\bilinear{\v, \v}} ~ \sqrt{\abs{a}^2\bilinear{\v, \v}} \\
    %                             & = \sqrt{\bilinear{\v, \v}} ~ \sqrt{a \bar{a}\bilinear{\v, \v}} \\
    %                             & = \sqrt{\bilinear{\v, \v}} ~ \sqrt{\bilinear{a\v, a\v}}        \\
    %                             & = \norm{\v} ~ \norm{a \v}                                      \\
    %                             & = \norm{\v} ~ \norm{\w}                                        \\
    %     \abs{\bilinear{\v, \w}} & \le \norm{\v} ~ \norm{\w}
    % \end{align*}
    % Case 2 ($\v, \w$ are linearly independent):\\
    % We can observe $\w \ne a \v$ for all $a \in \C$. \\
    Let $\v, \w ~(\ne \0) \in V$. Define $\u := \v - a \w$ for some $a \in \C$. % Since $\v, \w$ are linearly independent so $\u \ne \0$.\\
    Since $\bilinear{,}$ is a positive definite Hermitian form, we have $\bilinear{\u, \u} \ge 0$
    \begin{align*}
        \bilinear{\u, \u} & = \bilinear{\v - a \w, \v - a \w}                                                                   \\
                          & = \bilinear{\v, \v - a \w} - \bilinear{a \w, \v - a \w}                                             \\
                          & = \bilinear{\v, \v - a \w} - \bar{a} \bilinear{\w, \v - a \w}                                       \\
                          & = \bilinear{\v, \v} - \bilinear{\v, a \w} - \bar{a} \bilinear{\w, \v} + \bar{a} \bilinear{\w, a \w} \\
                          & = \bilinear{\v, \v} - a \bilinear{\v, \w} - \bar{a} \bilinear{\w, \v} + \abs{a}^2 \bilinear{\w, \w}
    \end{align*}
    choose $a = \frac{\bilinear{\w, \v}}{\bilinear{\w, \w}}$, since $\w \ne \0$ so $a$ is well-defined
    \begin{align*}
        \bilinear{\u, \u} & = \bilinear{\v, \v} -  \frac{\bilinear{\w, \v}}{\bilinear{\w, \w}} \bilinear{\v, \w} - \cancel{\frac{\bilinear{\v, \w}}{\bilinear{\w, \w}} \bilinear{\w, \v}} + \cancel{\frac{\bilinear{\v, \w} \bilinear{\w, \v}}{\bilinear{\w, \w}^2} \bilinear{\w, \w}} \\
                          & = \bilinear{\v, \v} - \frac{\bilinear{\v, \w} \bilinear{\w, \v}}{\bilinear{\w, \w}}                                                                                                                                                                        \\
                          & = \bilinear{\v, \v} - \frac{\bilinear{\v, \w} \overline{\bilinear{\v, \w}}}{\bilinear{\w, \w}}                                                                                                                                                             \\
                          & = \bilinear{\v, \v} - \frac{\abs{\bilinear{\v, \w}}^2}{\bilinear{\w, \w}} \ge 0
    \end{align*}
    From this inequality, we get
    \begin{align*}
        \abs{\bilinear{\v, \w}}^2       & \le \bilinear{\v, \v} ~ \bilinear{\w, \w}
        \intertext{taking square root both sides, we have}
        \Aboxed{\abs{\bilinear{\v, \w}} & \le \norm{\v} ~ \norm{\w}}
    \end{align*}
    And equality holds if and only if $\w = a \v$ for some $a \in \C$.
\end{proof}

\begin{exercisebox}
    \begin{exercise}[7+8+10 points]
        Let $\P_3(t)$ denote the space of polynomials in $t$ with real coefficients and of degree less than or equal to 3. Consider the symmetric bilinear $s: \P_3(t) \times \P_3(t) \to \R$ which is given by
        \[
            s(f, g) := \int_{-1}^{1} f(t) g(t) \dd{t}
        \]
        \begin{enumerate}[(a)]
            \item Write down the Gram matrix of $s$ with respect to the basis $\qty{1, t, t^2, t^3}$ of $\P_3(t)$. \vspace{-1.25ex}
            \item Show that the form $s$ is positive definite. \vspace{-1.25ex}
            \item Find an orthonormal basis of $\P_3(t)$ with respect to the form $s$.
        \end{enumerate}
    \end{exercise}
\end{exercisebox}

\begin{proof}
    Define the given basis as $\B := \qty{1, t, t^2, t^3} = \qty{\v_1, \v_2, \v_3, \v_4}$.
    \begin{enumerate}[(a)]
        \item Then the Gram Matrix $A$ for bilinear form $s$ is given by
              \begin{align*}
                  A           & = \mqty(
                  s(1,1)      & s(1, t)     & s(1,t^2)    & s(1, t^3)   \\
                  s(t,1)      & s(t, t)     & s(t,t^2)    & s(t, t^3)   \\
                  s(t^2,1)    & s(t^2, t)   & s(t^2,t^2)  & s(t^2, t^3) \\
                  s(t^3,1)    & s(t^3, t)   & s(t^3,t^2)  & s(t^3, t^3)
                  )                                                     \\
                              & = \mqty(
                  2           & 0           & \frac{2}{3} & 0           \\
                  0           & \frac{2}{3} & 0           & \frac{2}{5} \\
                  \frac{2}{3} & 0           & \frac{2}{5} & 0           \\
                  0           & \frac{2}{5} & 0           & \frac{2}{7}
                  )
              \end{align*}

        \item It is enough to show that this Gram matrix $A$ is positive definite. Let a arbitrary polynomial $p(t) (\ne 0) \in \P_3(t)$.
              % We can write $p(t) = a_0 + a_1 t + a_2 t^2 + a_3 t^3$ for some non-zero reals $a_0, a_1, a_2, a_3$. 
              So, we have to show that $s(p, p) > 0$ for all non-zero $p \in \P_3(t)$ and $0$ if and only if $p = 0$. Using the basis we can write $p(t) = [\B] \vb{x}$ for some $\vb{x} = \mqty(a_0 & a_1 & a_2 & a_3)\trans \in \R^4$. Now, we have
              \begin{gather*}
                  s(p, p)     = \vb{x}\trans A \vb{x}                                           \\
                  = \mqty(a_0             & a_1         & a_2         & a_3) \mqty(
                  2           & 0                       & \frac{2}{3} & 0                         \\
                  0           & \frac{2}{3}             & 0           & \frac{2}{5}               \\
                  \frac{2}{3} & 0                       & \frac{2}{5} & 0                         \\
                  0           & \frac{2}{5}             & 0           & \frac{2}{7}
                  ) \mqty(a_0                                                                     \\ a_1 \\ a_2 \\ a_3) \\
                  \Bilinear{p, p} = \textcolor{red}{2 a_0^2}+\textcolor{red}{\frac{4 a_2 a_0}{3}}+\frac{2 a_1^2}{3}+\textcolor{red}{\frac{2 a_2^2}{5}}+\frac{2 a_3^2}{7}+\frac{4 a_1 a_3}{5} \\
                  = \textcolor{red}{2 \qty(a_0 + \frac{a_2}{3})^2 + \frac{8}{45} a_2^2} + 2 \qty(\frac{a_1}{\sqrt{3}} + \frac{\sqrt{3} a_3}{5})^2 + \frac{8}{175} a_3^2
              \end{gather*}
              Note, $\Bilinear{p, p} \ge 0$ is given by the sum of squares. So,
              \[
                  \Bilinear{p, p} \ge 0
              \]
              \begin{claim}
                  $\Bilinear{p, p} = 0 \iff p(t) = 0$
              \end{claim}
              \begin{Proof}
                  The converse of this statement is trivial.  \\
                  We have to show that $\Bilinear{p, p} = 0 \implies p(t) = 0$.
                  \begin{gather*}
                      \Bilinear{p, p} = 0 \\
                      \implies \quad a_0 + \frac{a_2}{3} = 0 ~;~ a_2 = 0 ~;~ \frac{a_1}{\sqrt{3}} + \frac{\sqrt{3} a_3}{5} = 0 ~;~ a_3 = 0 \\
                      \implies a_0 = a_1 = a_2 = a_3 = 0
                  \end{gather*}
                  This proves our claim.
              \end{Proof}

        \item Since we have proved that this bilinear form is positive definite. So, the norm is well-defined \ie this space with the given bilinear form is normed vector space. Now, this enables us to apply Gram-Schmidt Orthogonalization. Define the orthonormal basis be $\hat{\B} := \qty{\e{1}, \e{2}, \e{3}, \e{4}}$ by
              \begin{gather*}
                  \e{1} := \frac{\v_{1}}{\norm{\v_{1}}} \\
                  \vb{e}_{i} := \v_i - \sum_{j = 1}^{i-1} \bilinear{\v_i, \e{j}}~ \e{j} \qquad \& \qquad \e{i} := \frac{\vb{e}_{i}}{\norm{\vb{e}_{i}}} \qquad \forall i \in \qty{2, 3, \dots}
              \end{gather*}
              \begin{enumerate}[1.]
                  \item Here, we can directly computes $\e{1} = \frac{1}{\sqrt{2}}$.
                  \item Here, we first compute $\vb{e}_{2} = \v_2 - \bilinear{\v_2, \e{1}} \e{1} ~\implies~ \vb{e}_2 = t - 0 (1) = t$. Now, we normalise this polynomial, $\e{2} = \frac{t}{\norm{\vb{e}_2}} = \sqrt{\frac{3}{2}} t$.
                  \item $\vb{e}_3 = \v_3 - \bilinear{\v_3, \e{2}} \e{2} - \bilinear{\v_3, \e{1}} \e{1} = t^2 - 0\qty(\sqrt{\frac{3}{2}} t) - \frac{\sqrt{2}}{3} \frac{1}{\sqrt{2}} = t^2 - \frac{1}{3}$. Now, we normalize this polynomial, $\e{3} = \sqrt{\frac{5}{8}} \qty(3 t^2 - 1)$.
                  \item $\vb{e}_4 = \v_4 - \bilinear{\v_4, \e{3}} \e{3} - \bilinear{\v_4, \e{2}} \e{2} - \bilinear{\v_4, \e{1}} \e{1} = t^3 - \frac{\sqrt{6}}{5} \sqrt{\frac{3}{2}} t = t^3 - \frac{3}{5}t$. Now, we normalize this polynomial, $\e{4} = \sqrt{\frac{7}{8}} \qty(5 t^3 - 3 t)$.
              \end{enumerate}
              Now, we have an orthonormal basis for this inner product space.
              \[
                  \hat{\B} = \qty{\frac{1}{\sqrt{2}}, \sqrt{\frac{3}{2}} t, \sqrt{\frac{5}{8}} \qty(3 t^2 - 1), \sqrt{\frac{7}{8}} \qty(5 t^3 - 3 t)}
              \]
    \end{enumerate}
\end{proof}

\begin{exercisebox}
    \begin{exercise}[10 points]
        Let $A$ be an $n \times n$ matrix whose diagonal entries are $n - 1$, and all off-diagonal entries are $-1$. Show that $A$ is a positive semidefinite matrix.
    \end{exercise}
\end{exercisebox}

\begin{proof}
    From the given description of $A$, we have,
    \[
        A_{ij} = \begin{cases}
            n-1 \quad & i = j            \\
            -1 \quad  & \text{otherwise}
        \end{cases} \quad \implies \quad
        A = % \mqty(
        % n-1 & -1 & \cdots & -1 \\
        % -1 & n-1 & \cdots & -1 \\
        % \vdots & \ddots & {} & \vdots \\
        % -1 & \cdots & -1 & n-1
        % )
        \mqty(\dmat[-1]{n-1, \ddots, n-1})_{n \times n}
    \]
    It is enough to show that for all $\vb{x} = \mqty(x_1 & x_2 & \cdots & x_n)\trans \in \R^n$, we have
    \[
        \vb{x}\trans A \vb{x} \ge 0.
    \]
    From the normal matrix multiplication, we have
    \begin{gather*}
        \vb{x}\trans A \vb{x} = \mqty(x_1 & x_2 & \cdots & x_n) \mqty(\dmat[-1]{n-1, \ddots, n-1}) \mqty(x_1 \\ x_2 \\ \vdots \\ x_n) \\
        \vb{x}\trans A \vb{x} = \mqty(x_1 & x_2 & \cdots & x_n) \mqty(n x_1 - \sum x_i \\ n x_2 - \sum x_i \\ \vdots \\ n x_n - \sum x_i) \\
        \vb{x}\trans A \vb{x} = \qty(n x_1^2 - x_1 \sum x_i) + \qty(n x_2^2 - x_2 \sum x_i) + \cdots + \qty(n x_n^2 - x_n \sum x_i) \\
        \boxed{\vb{x}\trans A \vb{x} = n \sum x_i^2 - \qty(\sum x_i)^2}
    \end{gather*}
    Note the sum is running from $i = 1$ to $n$ (explicitly not mentioned in the calculation).
    \begin{claim}
        $n \sum x_i^2 - \qty(\sum x_i)^2 = n \sum (x_i - \bar{x})^2$, where $n \bar{x} := \sum x_i$.
    \end{claim}
    \begin{Proof}
        Observe,
        \[
            \qty(\sum x_i)^2 = n^2 (\bar{x})^2
        \]
        using this observation, we have
        \begin{align*}
            n \sum x_i^2 - \qty(\sum x_i)^2 & = n \qty(\sum x_i^2 - n (\bar{x})^2)                         \\
                                            & = n \qty(\sum x_i^2 - 2 n (\bar{x})^2 + n (\bar{x})^2)       \\
                                            & = n \qty(\sum x_i^2 - 2 \bar{x} \sum x_i + n (\bar{x})^2)    \\
                                            & = n \qty(\sum x_i^2 - 2 \bar{x} \sum x_i + \sum (\bar{x})^2) \\
                                            & = n \qty(\sum x_i^2 - \sum 2 \bar{x} x_i + \sum (\bar{x})^2) \\
                                            & = n \sum \qty(x_i^2 - 2 \bar{x} x_i + (\bar{x})^2)           \\
                                            & = n \sum (x_i - \bar{x})^2
        \end{align*}
    \end{Proof}
    Now, $\vb{x}\trans A \vb{x} = n \sum (x_i - \bar{x})^2$, since this is the sum of squares times $n \in \N$, so
    \[
        \vb{x}\trans A \vb{x} \ge 0.
    \]
\end{proof}
\end{document}