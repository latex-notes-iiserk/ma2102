\documentclass{scrartcl}
\usepackage{handout, xargs}

%%%% MACROS %%%%
\newcommandx{\M}[2][2 = {}]{\mathcal{M}_{#1 #2}}
\newcommand{\F}{\mathbb{F}}
\newcommand{\I}[1]{\mathbb{I}_{#1}}
\DeclareMathOperator{\col}{col}
\DeclareMathOperator{\row}{row}
\renewcommand*{\implies}{\Rightarrow}
\newcommand{\B}{\mathbb{B}}
\DeclareMathOperator{\Span}{span}


%%%% TITLE %%%%
\title{
    \huge\textbf{Class Notes} \\
    \vspace{10pt}
}

\subtitle{
    \LARGE MA2102: Linear Algebra I \\[5pt]
    \large Autumn 2023
    \vspace{10pt}
}

\author{
    Debayan Sarkar
        \thanks{{\href{https://thesillycoder.github.io}{TheSillyCoder.github.io}}}
        \\\href{mailto:ds22ms002@iiserkol.ac.in}{22MS002} \and
    Gautam Singh
        % \thanks{{}}
        \\\href{mailto:gs22ms023@iiserkol.ac.in}{22MS023} \and
    Piyush Kumar Singh
        \thanks{{\href{https://iampiyushkrsingh.github.io}{iamPiyushKrSingh.github.io}}}
        \\\href{mailto:pks22ms027@iiserkol.ac.in}{22MS027} \and
    Sabarno Saha
        % \thanks{{}}
        \\\href{mailto:ss22ms037@iiserkol.ac.in}{22MS037}
}

\date{
    \normalsize
    \textit{Indian Institute of Science Education and Research, Kolkata, \\
        Mohanpur, West Bengal, 741246, India.}
}

\begin{document}
\maketitle
\tableofcontents

\section{Prerequisites}

\subsection{Binary Operation}
Let a non-empty set $S$, and a map $f$ is defined as
\begin{gather*}
    f: S \times S  \longrightarrow S,                                  \\
    (s, s')        \longmapsto f(s, s') \in S, ~\forall ~ s, s' \in S.
\end{gather*}

\subsection{Group}

\begin{definition}[Group]
    A set $G$ with a binary operation $*$ is called a \vocab{Group} and denoted by $(G, *)$ \emph{if and only if} it satisfies the following axioms:
    \begin{enumerate}[label=\textbf{A{\arabic*}.}]
        \item (\emph{Associativity}) For all $a, b, c \in G$, $a * (b * c) = (a * b) * c = a * b * c$.
        \item (\emph{Identity}) There is an element called the \vocab{identity} and sometimes denoted by $i_{G}$ such that
              \begin{equation*}
                  \forall ~ a \in G \qc{i_G * a = a = a * i_G}.
              \end{equation*}
        \item (\emph{Inverse}) For each element $a \in G$ there exists $b \in G$ such that
              \begin{equation*}
                  a * b = b * a = i_G.
              \end{equation*}
    \end{enumerate}
\end{definition}
\begin{problem}{classEx}
\item Prove the uniqueness of the identity element \ie there exists a unique identity associated to binary operator $*$ in $G$.
\item Prove that there exists a unique inverse for each $a \in G$.
\end{problem}

\subsubsection*{Subgroups}
\begin{definition}[Subgroups]
    Let $(G, *)$ is a group and $H \subseteq G ~(\ne \emptyset)$ is called a subgroup of $G$ \emph{if and only if} following axioms are satisfies
    \begin{enumerate}[label=\textbf{\arabic*}]
        \item (\emph{Closure}) $* : H \times H \longrightarrow H$.
        \item (\emph{Identity}) The identity element of $G$ is in $H$ \ie $i_G \in H$.
        \item (\emph{Inverse}) Inverse of each element in $H$ also belongs to H.
    \end{enumerate}
    $$\fbox{\text{OR}}$$
    $(H, *)$ is a group and $H \subseteq G ~(\ne \emptyset)$.
\end{definition}

\begin{definition*}[Semigroup]
    A set with a law of composition such that ``Associativity'' is satisfied.
\end{definition*}
\noindent\eg $(\N, +)$

\begin{definition*}[Monoid]
    A semigroup with an identity element.
\end{definition*}
\noindent\eg $(\N, \times)$

\begin{example}[Finite Group]
    A group with a finite number of elements
    \begin{itemize}[label = \textbf{E.g.}]
        \item $\qty(\qty{0, 1}, \oplus)$, the operation here is `binary addition.'
              \begin{align*}
                  1 \oplus 1 & = 0 \\
                  0 \oplus 1 & = 1 \\
                  1 \oplus 0 & = 1 \\
                  0 \oplus 0 & = 0
              \end{align*}
        \item $(\Z/12\Z, \oplus_{12})$, observe that this operation is not the same as an addition, but this is special addition as follows
              \begin{align*}
                  10 \oplus_{12} 10 & = 8 \\
                  4 \oplus_{12} 4   & = 8
              \end{align*}
              And here the set $\Z/12\Z = \qty{\bar{1}, \bar{2}, \cdots, \bar{11}}$ and $\bar{n} \in \Z/12\Z$ is nothing but a residue class of $n$.
    \end{itemize}
\end{example}
\subsubsection{Abelian Group}
\begin{definition}[Abelian Group]
    Let a group $(G, *)$ such that, for all $a, b \in G$
    \begin{equation*}
        a * b = b * a
    \end{equation*}
    we say $*$ is commutative operator and $G$ is a commutative group or ``\vocab{abelian group}.''
\end{definition}

\subsubsection{Ring}
\begin{definition}
    Let a set $R ~(\ne \emptyset)$ and two binary operation
    \begin{enumerate}[(i)]
        \item addition ($+$), and
        \item multiplication ($*$)
    \end{enumerate}
    defined on $R$, then we call $R$ a ring and denote it by $(R, + ,*)$ \emph{if and only if} the following is satisfied
    \begin{itemize}[$\cdot$]
        \item $(R, +)$ is an abelian group. And the additive identity is denoted by $0_R$.
        \item $(R, *)$ is a monoid. And the multiplicative identity is denoted by $1_R$.
        \item multiplication is distributive  over addition
              \begin{gather*}
                  a * (b + c) = a * b + a * c \\
                  (b + c) * a = b * a + c * a
              \end{gather*}
    \end{itemize}
    \noindent \eg $\M{n}(\R) = \qty{n \times n ~\text{matrices over}~ \R}$, $\Z/n\Z$.
\end{definition}

\begin{definition*}[Commutative Ring]
    A ring $(R, +, *)$ where $*$ is commutative.

    \noindent \eg $\Z$
\end{definition*}

\subsubsection{Field}
\begin{definition}[Field]
    A ring $\F$ is called a ``\vocab{Field}'' \emph{if and only if} $\F^* := (\F \setminus \qty{0}, *)$ is an abelian group.

    \noindent \eg $\Q$, $\R$, $\C$
\end{definition}

\begin{problem*}{classEx}
    \item Prove $\Z/p\Z$ is a field \emph{iff} $p$ is a prime.
\end{problem*}

\end{document}